


let wrestlers = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];
console.log("Original Array:");
console.log(wrestlers);

// Problem 1:
function addSingleArgument(value){
	wrestlers[wrestlers.length]=value;
}

addSingleArgument("John Cena");
console.log(wrestlers);

// Problem 2
let itemFound;
function returnSingleArgument(index){
	return wrestlers[index];
}

itemFound = returnSingleArgument(2);
console.log(itemFound)

// Problem 3
function deleteLastIndex(){
	let lastItem = wrestlers[wrestlers.length-1];
	wrestlers.length--;
	return lastItem;
}
let deletedItem = deleteLastIndex();
console.log(deletedItem);
console.log(wrestlers);

// Problem 4
function updateArray(index, value) {
	wrestlers[index] = value;
}
updateArray(3, "Triple H");
console.log(wrestlers);

// Problem 5
function deleteInsideOfArray(){
	let length = wrestlers.length;
	for(let index = 0; index<length; index++){
		wrestlers.length--;	
	}
	
}
deleteInsideOfArray();
console.log(wrestlers);


// Problem 6
function checkEmptyArray(){
	if(wrestlers.length>0){
		return false;
	}
	else{
		return true;
	}
}
let isUsersEmpty = checkEmptyArray();
console.log(isUsersEmpty);