

//ARRAYS
/*
	An array in programming is simply a list of data
	They are declared using square brackets
	They are used to store numerous amounts of data to manipulate in order to perform a number of tasks.
*/

let studentNumberA= '2023-1923'
let studentNumberB= '2023-1924'
let studentNumberC= '2023-1925'
let studentNumberD= '2023-1926'
let studentNumberE= '2023-1927'

//With array
let studentNumbers = ['2023-1928', '2023-1929', '2023-1930',];

let grades = [98.5, 94.3, 99.01, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]

//Not applicable on strings
let fullName = "john gabryl"
fullName.length = fullName.length - 1;
console.log(fullName);

//Array indexing: 
console.log(grades[0]);

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegend[1]);
console.log(lakersLegend[3]);

let basketPlayer1 = lakersLegend[1];
console.log(basketPlayer1);

let lastElement = lakersLegend[lakersLegend.length -1];
console.log(lastElement);


let newArr = ['john', 'gab', 'j','g']
// looping over an array
for(let index =0; index< newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];

for(let index=0; index < numArr.length; index++)
{
	if(numArr[index]%5===0){
		console.log(numArr[index] + " is divisible by 5")
	}else{
		console.log(numArr[index]+ " is not divisible by 5")
	}
}